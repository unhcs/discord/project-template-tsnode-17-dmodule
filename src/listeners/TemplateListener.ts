import { 
  Module,
  Listener 
} from "@unh-csonline/discord-modules";
import { Message } from "discord.js";

export class TemplateListener extends Listener<"messageCreate"> {
  constructor(module: Module) {
      super(module, "messageCreate");

      console.info("INFO: Constructed discord-modules-attendance ReactionListener.");
  }

  public async execute(message: Message): Promise<void> {
    console.log(`Message created at ${message.createdAt}`);
  }
}