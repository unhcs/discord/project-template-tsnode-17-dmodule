import { GuildApplicationCommand, GuildApplicationCommandOptions, Module } from "@unh-csonline/discord-modules";
import { SlashCommandBuilder } from "@discordjs/builders";
import { CommandInteraction, MessageEmbed } from "discord.js";

export class TemplateCommand extends GuildApplicationCommand {
    constructor(module: Module) {
        super(module, {
            guild: module.client.guilds.cache.get(process.env.GUILD_ID as string),
            data: new SlashCommandBuilder()
                .setName("template")
                .setDescription("Template command description")       
        } as GuildApplicationCommandOptions);
    }

    public async execute(interaction: CommandInteraction): Promise<void> {
        const error = false;
        if(error)
            throw "Template error occured";

        await interaction.reply({embeds: [new MessageEmbed().setDescription("Template reply.")]}).catch(console.error);
        return;
    }
}