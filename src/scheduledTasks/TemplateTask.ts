import { Module, ScheduledTask } from "@unh-csonline/discord-modules";

export class TemplateTask extends ScheduledTask {
    constructor(module: Module){
        //runs whenever the second is 0, 15, 30, or 45
        super(module, "templateTask", "0,15,30,45 * * * * *");
    }

    public async execute (){
        const guild = await this.module.client.guilds.fetch(process.env.GUILD_ID as string);
        const channels = await guild.channels.fetch();

        console.log(`executing template task. There are ${channels.size} channels in the server.`);
    }
}