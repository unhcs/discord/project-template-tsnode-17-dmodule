import typescript from "@rollup/plugin-typescript";

export default {
    input: "src/index.ts",
    output: {
        file: "build/index.js",
        sourcemap: "inline",
        format: "cjs",
        exports: "default"
    },
    watch: {
        include: "src/**"
    },
    external: [
        "@unh-csonline/discord-modules",
        "@discordjs/builders",
        "discord.js"
    ],
    plugins: [
        typescript({
            tsconfig: "tsconfig.json"
        }),
    ]
};