# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.1.0](https://gitlab.com/unhcs/discord/project-template-tsnode-17-dmodule/compare/v2.0.0...v2.1.0) (2023-09-22)


### Features

* add template for scheduled tasks ([580ec06](https://gitlab.com/unhcs/discord/project-template-tsnode-17-dmodule/commit/580ec066b36e2816c422c9b5566223399058758d))

## [2.0.0](https://gitlab.com/unhcs/discord/project-template-tsnode-17-dmodule/compare/v1.2.0...v2.0.0) (2023-08-25)


### Features

* replace discord-modules dependency with unh version ([b3b6dbe](https://gitlab.com/unhcs/discord/project-template-tsnode-17-dmodule/commit/b3b6dbea4a3b190e9383e9c721af549315716ec4))
* update template for discord-modules specific usage ([7fdc032](https://gitlab.com/unhcs/discord/project-template-tsnode-17-dmodule/commit/7fdc0322923374714ae293b084acef1bd7f1c3af))

## 1.2.0 (2022-03-14)

## 1.1.0 (2022-03-14)

### Features

-   add vscode workspace settings ([ffa64c7](https://gitlab.com/unh-cs-department/project-template-tsnode17/commit/ffa64c7ca235c45531ad48914d8191209e7b9c4a))

### 1.0.0 (2022-03-14)
